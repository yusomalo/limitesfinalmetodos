
$(document).ready(function(){
  $("#result").hide();
  $("#Z").fadeOut();
  $("#Dave").fadeOut();
  $('#Lambda').fadeOut('slow');
  var D = parseFloat($('#d').val());
  var Dmax = parseFloat($('#dmax').val());
  var Dave = parseFloat($('#dave').val());
  var N = parseFloat($('#n').val());
  var Z = parseFloat($('#z').val());
  var workload =  $('#workload').val();
  var output = $('#output').val();
  var bound = $('#bounds').val();
  var formula = "";
  var chartFunction;
  var chartDemo = 0;

  $('.form-group').change(function() {
    workload =  $('#workload').val();
    output = $('#output').val();
    bound = $('#bounds').val();

    if (workload == "Transaction") {
      transaction(workload, output, bound);
    }
    else if (workload == "Batch") {
      batch(bound);
    }
    else {
      terminal(bound);
    }

  });

  $("#generate").click(function(){
    $("#leftBound").empty();
    $("#boundResult").empty();
    $("#rightBound").empty();
    D = parseFloat($('#d').val());
    Dmax = parseFloat($('#dmax').val());
    Dave = parseFloat($('#dave').val());
    N = parseFloat($('#n').val());
    Z = parseFloat($('#z').val());
    var lambda = parseFloat($('#lambda').val());
    var resultFinal = new Array();
    var resultType = "";
    var units = "";
    $(".left").fadeIn('slow');
    $(".right").fadeIn('slow');
    if (output == "Throughput (X)") {
      resultType = `X(${N})`;
      units = `(tasks/s)`;
      if (formula == "Abatch"){
        resultFinal = batchAsymptoticX(D, N, Dmax, Z, Dave, lambda);
        chartFunction = batchAsymptoticX;
      }
      else if (formula == "Aterminal") {
        resultFinal = terminalAsymptoticX(D, N, Dmax, Z, Dave, lambda);
        chartFunction = terminalAsymptoticX;
      }
      else if (formula == "Atransaction") {
        N = lambda;
        resultFinal = transactionAsymptoticX(D, N, Dmax, Z, Dave, lambda);
        resultType = `X(λ): X(${lambda})`;
        $(".left").fadeOut('slow');
        chartFunction = transactionAsymptoticX;
      }
      else if (formula == "Bbatch") {
        resultFinal = batchBalancedX(D, N, Dmax, Z, Dave, lambda);
        chartFunction = batchBalancedX;
      }
      else if (formula == "Bterminal") {
        resultFinal = terminalBalancedX(D, N, Dmax, Z, Dave, lambda);
        chartFunction = terminalBalancedX;
      }
      else if (formula == "Btransaction") {
        N = lambda;
        resultFinal = transactionBalancedX(D, N, Dmax, Z, Dave, lambda);
        $(".left").fadeOut('slow');
        resultType = `X(λ): X(${lambda})`;
        chartFunction = transactionBalancedX;
      }
    }
    else {
      resultType = `R(${N})`;
      units = `(s)`;
      if (formula == "Abatch"){
        resultFinal = batchAsymptoticR(D, N, Dmax, Z, Dave, lambda);
        chartFunction = batchAsymptoticR;
      }
      else if (formula == "Aterminal") {
        resultFinal = terminalAsymptoticR(D, N, Dmax, Z, Dave, lambda);
        chartFunction = terminalAsymptoticR;
      }
      else if (formula == "Atransaction") {
        N = lambda;
        resultFinal = transactionAsymptoticR(D, N, Dmax, Z, Dave, lambda);
        $(".right").fadeOut('slow');
        resultType = `R(λ): R(${lambda})`;
        chartFunction = transactionAsymptoticR;
      }
      else if (formula == "Bbatch") {
        resultFinal = batchBalancedR(D, N, Dmax, Z, Dave, lambda);
        chartFunction = batchBalancedR;
      }
      else if (formula == "Bterminal") {
        resultFinal = terminalBalancedR(D, N, Dmax, Z, Dave, lambda);
        chartFunction = terminalBalancedR;
      }
      else if (formula == "Btransaction") {
        N = lambda;
        resultFinal = transactionBalancedR(D, N, Dmax, Z, Dave, lambda);
        resultType = `R(λ): R(${lambda})`;
        chartFunction = transactionBalancedR;
      }
    }
    $("#leftBound").append(resultFinal[0].toFixed(3) + units);
    $("#boundResult").append(resultType);
    $("#rightBound").append(resultFinal[1].toFixed(3) + units);
    $("#result").fadeIn('slow');
    createChart();
  });

function batch(bound) {
        $('#Lambda').fadeOut('slow');
        if (bound == "Balanced System Bounds") {
          $('#D').fadeIn('slow');
          $('#Dmax').fadeIn('slow');
          $('#N').fadeIn('slow');
          $('#Dave').fadeIn('slow');
          $('#Z').fadeOut('slow');
          formula = "Bbatch"
        } else {
          $('#D').fadeIn('slow');
          $('#Dmax').fadeIn('slow');
          $('#N').fadeIn('slow');
          $('#Dave').fadeOut('slow');
          $('#Z').fadeOut('slow');
          formula = "Abatch"
        }
  }

function terminal(bound) {
        $('#Lambda').fadeOut('slow');
        if (bound == "Balanced System Bounds") {
            $('#D').fadeIn('slow');
            $('#Dmax').fadeIn('slow');
            $('#N').fadeIn('slow');
            $('#Dave').fadeIn('slow');
            $('#Z').fadeIn('slow');
            formula = "Bterminal"
          } else {
            $('#D').fadeIn('slow');
            $('#Dmax').fadeIn('slow');
            $('#N').fadeIn('slow');
            $('#Dave').fadeOut('slow');
            $('#Z').fadeIn('slow');
            formula = "Aterminal"
          }
}

function transaction(workload, output, bound) {
    $('#Lambda').fadeIn('slow');
    if(output == "Throughput (X)"){
      $('#Dmax').fadeIn('slow');
      $('#D').fadeOut('slow');
      $('#N').fadeOut('slow');
      $('#Dave').fadeOut('slow');
      $('#Z').fadeOut('slow');
      formula = bound == "Balanced System Bounds" ? "Btransaction" : "Atransaction";
    } else if (output == "Response Time (R)"){
        if (bound == "Balanced System Bounds") {
          $('#D').fadeIn('slow');
          $('#Dmax').fadeIn('slow');
          $('#N').fadeOut('slow');
          $('#Dave').fadeIn('slow');
          $('#Z').fadeOut('slow');
          formula = "Btransaction";
        } else {
          $('#D').fadeIn('slow');
          $('#Dmax').fadeOut('slow');
          $('#N').fadeOut('slow');
          $('#Dave').fadeOut('slow');
          $('#Z').fadeOut('slow');
          formula = "Atransaction";
        }
    }
}

/*************ASYMPTOTIC****************/
function batchAsymptoticX (D, N, Dmax, Z, Dave, lambda) {
  var resultBatch = new Array();
  var leftBound = (1/D);
  resultBatch.push(leftBound);

  var rightBound = Math.min(N/D, 1/Dmax);
  resultBatch.push(rightBound);


  return resultBatch;
}

function terminalAsymptoticX (D, N, Dmax, Z, Dave, lambda) {
  var result = new Array();

  var leftBound = (N/((N*D) + Z));
  result.push(leftBound);

  var rightBound = Math.min((N/(D+Z)), (1/Dmax));
  result.push(rightBound);

  return result;
}

function transactionAsymptoticX (D, N, Dmax, Z, Dave, lambda) {
  var result = new Array();
  result.push(-100.0);
  result.push(1/Dmax);

  return result;
}


function batchAsymptoticR (D, N, Dmax, Z, Dave, lambda) {
  var result = new Array();

  var leftBound = Math.max(D, (N * Dmax));
  result.push(leftBound);


  var rightBound = N * D;
  result.push(rightBound);

  return result;
}

function terminalAsymptoticR (D, N, Dmax, Z, Dave, lambda) {
  var result = new Array();
  var leftBound = Math.max(D, (N*Dmax) - Z);
  result.push(leftBound);


  var rightBound = N * D;
  result.push(rightBound);

  return result;
}

function transactionAsymptoticR (D, N, Dmax, Z, Dave, lambda) {
  var result = new Array();
  var leftBound = D;
  result.push(leftBound);

  result.push(100.0);

  return result;
}


/*************BALANCED****************/
function batchBalancedX (D, N, Dmax, Z, Dave, lambda) {
  var result = new Array();

  var leftBound = (N/(D + ((N - 1) * Dmax)));
  result.push(leftBound);

  var rightBound = Math.min(1/Dmax, (N/(D + ((N - 1) * Dave))));
  result.push(rightBound);


  return result;
}

function terminalBalancedX (D, N, Dmax, Z, Dave, lambda) {
  var result = new Array();

  var leftBound = (N/(D + Z + (((N - 1) * Dmax)/(1 + Z/(N * D)))));
  result.push(leftBound);

  var rightBound = Math.min((1/Dmax), (N/(D + Z + (((N - 1) * Dave)/(1 + (Z/D))))));
  result.push(rightBound);

  return result;
}

function transactionBalancedX (D, N, Dmax, Z, Dave, lambda) {
  var result = new Array();
  result.push(1.0);
  result.push(1/Dmax);

  return result;
}


function batchBalancedR (D, N, Dmax, Z, Dave, lambda) {
  var result = new Array();

  var leftBound = Math.max((N * Dmax), (D + ((N - 1) * Dave)));
  result.push(leftBound);

  var rightBound = (D + ((N - 1) * Dmax));
  result.push(rightBound);

  return result;
}

function terminalBalancedR (D, N, Dmax, Z, Dave, lambda) {
  var result = new Array();

  var leftBound = Math.max((N * Dmax) - Z, (D + (((N - 1) * Dave)/(1 + Z/D))));
  result.push(leftBound);

  var rightBound = (D + (((N - 1) * Dmax)/(1 + Z/(N * D))));
  result.push(rightBound);

  return result;
}

function transactionBalancedR (D, N, Dmax, Z, Dave, lambda) {
  var result = new Array();
  var leftBound = (D/(1 - (N * Dave)));
  result.push(leftBound);

  var rightBound = (D/(1 - (N * Dmax)));
  result.push(rightBound);

  return result;
}

function createChart() {

  var originalLineDraw = Chart.controllers.line.prototype.draw;
Chart.helpers.extend(Chart.controllers.line.prototype, {
  draw: function() {
    originalLineDraw.apply(this, arguments);

    var chart = this.chart;
    var ctx = chart.chart.ctx;

    var index = chart.config.data.lineAtIndex;
    if (index) {
      var xaxis = chart.scales['x-axis-0'];
      var yaxis = chart.scales['y-axis-0'];

      ctx.save();
      ctx.beginPath();
      ctx.moveTo(xaxis.getPixelForValue(undefined, index), yaxis.top);
      ctx.strokeStyle = '#ff0000';
      ctx.lineTo(xaxis.getPixelForValue(undefined, index), yaxis.bottom);
      ctx.stroke();
      ctx.restore();
    }
  }
});

   var fillBetweenLinesPlugin = {
      afterDatasetsDraw: function (chart) {
          var ctx = chart.chart.ctx;
          var xaxis = chart.scales['x-axis-0'];
          var yaxis = chart.scales['y-axis-0'];
          var datasets = chart.data.datasets;
          ctx.save();

          for (var d = 0; d < datasets.length - 1; d++) {
              var dataset = datasets[d];
              if (dataset.fillBetweenSet == undefined) {
                  continue;
              }

              // get meta for both data sets
              var meta1 = chart.getDatasetMeta(d);
              var meta2 = chart.getDatasetMeta(dataset.fillBetweenSet);

              // do not draw fill if one of the datasets is hidden
              if (meta1.hidden || meta2.hidden) continue;

              // create fill areas in pairs
              for (var p = 0; p < meta1.data.length-1;p++) {
                  // if null skip
                if (dataset.data[p] == null || dataset.data[p+1] == null) continue;

                ctx.beginPath();

                // trace line 1
                var curr = meta1.data[p];
                var next = meta1.data[p+1];
                ctx.moveTo(curr._view.x, curr._view.y);
                ctx.lineTo(curr._view.x, curr._view.y);
                if (curr._view.steppedLine === true) {
                  ctx.lineTo(next._view.x, curr._view.y);
                  ctx.lineTo(next._view.x, next._view.y);
                }
                else if (next._view.tension === 0) {
                  ctx.lineTo(next._view.x, next._view.y);
                }
                else {
                    ctx.bezierCurveTo(
                      curr._view.controlPointNextX,
                      curr._view.controlPointNextY,
                      next._view.controlPointPreviousX,
                      next._view.controlPointPreviousY,
                      next._view.x,
                      next._view.y
                    );
                              }

                // connect dataset1 to dataset2
                var curr = meta2.data[p+1];
                var next = meta2.data[p];
                ctx.lineTo(curr._view.x, curr._view.y);

                // trace BACKWORDS set2 to complete the box
                if (curr._view.steppedLine === true) {
                  ctx.lineTo(curr._view.x, next._view.y);
                  ctx.lineTo(next._view.x, next._view.y);
                }
                else if (next._view.tension === 0) {
                  ctx.lineTo(next._view.x, next._view.y);
                }
                else {
                  // reverse bezier
                  ctx.bezierCurveTo(
                    curr._view.controlPointPreviousX,
                    curr._view.controlPointPreviousY,
                    next._view.controlPointNextX,
                    next._view.controlPointNextY,
                    next._view.x,
                    next._view.y
                  );
                }

                              // close the loop and fill with shading
                ctx.closePath();
                ctx.fillStyle = dataset.fillBetweenColor || "rgba(0,0,0,0.1)";
                ctx.fill();
              } // end for p loop
          }
      } // end afterDatasetsDraw
  }; // end fillBetweenLinesPlugin

  Chart.pluginService.register(fillBetweenLinesPlugin);

  var drawingPlugin = {
      beforeInit: function(chart) {
          var data = chart.config.data;
          for (var i = 0; i < data.datasets.length; i++) {
              for (var j = 0; j < data.labels.length + 10; j++) {
              	var fct = data.datasets[i].function,
                  	x = data.labels[j],
                  	y = fct(x);
                  data.datasets[i].data.push(y);
              }
          }
      }
  };
  Chart.pluginService.register(drawingPlugin);

  Chart.plugins.register({
     afterDatasetsDraw: function(chart) {
        if (chart.tooltip._active && chart.tooltip._active.length) {
           var activePoint = chart.tooltip._active[0],
              ctx = chart.ctx,
              y_axis = chart.scales['y-axis-0'],
              x = activePoint.tooltipPosition().x,
              topY = y_axis.top,
              bottomY = y_axis.bottom;
           // draw line
           ctx.save();
           ctx.beginPath();
           ctx.moveTo(x, topY);
           ctx.lineTo(x, bottomY);
           ctx.lineWidth = 2;
           ctx.strokeStyle = '#07C';
           ctx.stroke();
           ctx.restore();
        }
     }
  });

  var chartData = {
    labels: [...Array(N+5).keys()],
      datasets: [
        {
            label: "Left bound",
            function: function(x) {
              var chart = chartFunction(D, x, Dmax, Z, Dave, lambda);
              return chart[0];},
              //return x/(1.134 + ((x-1)*0.447)) },
            borderColor: "rgba(255, 206, 200, 1)",
            data: [],
            fill: false,
            steppedLine: false,
            tension: 0,
            fillBetweenSet: 1,
            fillBetweenColor: "rgba(5,5,255, 0.2)"
        },
        {
            label: "Right bound",
            function: function(x) {
              var chart = chartFunction(D, x, Dmax, Z, Dave, lambda);
              return chart[1]; },
            borderColor: "rgba(89, 206, 120, 1)",
            data: [],
            fill: false,
            steppedLine: false,
            tension: 0,
            fillBetweenSet: 1,
            fillBetweenColor: "rgba(5,5,255, 0.2)"
        },
      ],
      lineAtIndex: N
  };

  var chartOptions = {
      responsive: true,
      title: {
          display: true,
          text: 'Bounds'
      },
      scales: {
          yAxes: [{
              ticks: {
                  beginAtZero:true
              }
          }],
      },
      tooltips: {
        enabled: true,
        mode: 'index',
        intersect: false
      },


  };

  if (chartDemo)
    chartDemo.destroy();

  chartDemo = new Chart($('#myChart').get(0), {
      type: 'line',
      data: chartData,
      options: chartOptions,
  });
}

});
