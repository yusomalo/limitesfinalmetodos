var express = require('express');
var app = express();
var Chart = require('chart.js');

app.use(express.static("public"))

app.get('/index', function (req, res) {
  res.sendfile('public/index1.html');
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
