//Express Configuration
const express = require('express');
const path = require('path');
const app = express();
const PORT = process.env.PORT || 3000;
const bodyParser = require('body-parser');

// Set static files
app.use(express.static(path.join(__dirname, '/public')));
app.use(bodyParser.json());
app.use(cookieParser());

// view engine setup
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname + '/public/view'));


const validateFirebaseIdToken = (req, res, next) => {
  //console.log('Checking if request is authorized with Firebase ID token');

  if ((!req.headers['boa-token']) && !(req.cookies.__session)) {
    console.error('No Firebase ID token was passed as a BOA token in the Authorization header.',
        'Make sure you authorize your request by providing the following HTTP header:',
        'Authorization: BOA <Firebase ID Token>',
        'or by passing a "__session" cookie.');
    //res.status(403).send('Unauthorized');
    res.redirect('../../login');
    return;
  }
  else {
    let idToken;
    if (req.headers['boa-token']) {
      //console.log('"Authorization" in header');
      // Read the ID Token from the Authorization header.
      idToken = req.headers['boa-token'];
    } else if(req.cookies.__session) {
      //console.log('"__session" cookie');
      // Read the ID Token from cookie.
      idToken = req.cookies.__session;
    } else {
      // No cookie
      res.redirect('../../login');
      //res.status(404).send('No autorizado');
      return;
    }
   //console.log("cookies", req.cookies);
  admin.auth().verifyIdToken(idToken)
  .then(function(decodedToken) {
    var uid = decodedToken.uid;
    if (uid) {
      //res.redirect('/index');
      next();
    }
    else {
      res.redirect('../../login');
    }
    // ...
  }).catch(function(error) {
    // Handle error
      res.redirect('../../login');
      console.log("not verified: ", error);
  });
  }
}
app.use('/login', login);

app.use(validateFirebaseIdToken);

app.get('/index', function(req, res) {
    var users = {
      'mau':
      {
        id: 'A01371473',
        name: 'Mauricio Maximiliano Pérez Pérez'
      },
      'yuso':
      {
        id: 'A01371454',
        name: 'José Antonio Malo de la Peña'
      },
      'guille':
      {
        id: 'A01377162',
        name: 'Guillermo Pérez Trueba'
      }
    }

    console.info(users);
    res.status(200);
    res.render('index.ejs', {
      'users': users,
    });
});

//Routing
app.get('/', function(req, res) {
   res.redirect('../../index')
});

app.use((req, res, next) => {console.log('Inside Middleware'); next();});

app.use('/soporte', problems);
app.use('/promos', promos);
app.use('/users', users);
app.use('/inventory', inventory);
app.use('/admin', main);

//app.use("/logout", main);

//INSERTS
app.get('/insert-product', (req, res) => {
  console.info(firebase);
  const rootRef = firebase.ref();
  const adaRef = firebase.ref("inventory/");
  // we can also chain the two calls together
  adaRef.push().set({
    "name" : "Mona Lisa",
    "author" : "Leonardo DaVinci",
    "cost" : "17 000",
    "period" : "Renassaince",
    });
    adaRef.push().set({
      "name" : "The Starry Night",
      "author" : "Vincent van Gogh",
      "cost" : "25 000",
      "period" : "Impressionism",
    });
    adaRef.push().set({
      "name" : "Guernica",
      "author" : "Pablo Picasso",
      "cost" : "250 000",
      "period" : "Modernism",
    });
    console.log('saved');
});


app.get('/insert', (req, res) => {
  console.info(firebase);
  const rootRef = firebase.ref();

  var adaRef = firebase.ref("sales/");
  adaRef.set({
    "1" : {
      "product" : "LQzbuopQz7HYziOoJWw",
      "date" : "2018-05-69",
      "admin" : "b305XwnX5QZLl2dFit2gPOJR36i2",
      "sale" : "35 600",
      "promo" : "1",
      "user" : "7nTnYvxd1ndjDVlPh3IFMc04Nrr2"
    },
    "2" : {
      "product" : "LRFYtrbMCNqcD5MA5ao",
      "date" : "2018-05-12",
      "admin" : "hfsP6GJrnzOX3MnmvoMofbuRFUv1",
      "sale" : "35 600",
      "promo" : "0",
      "user" : "Aub75KQPO0efGY2DJCWW9s02IPG3"
    },
  });

  var adaRef = firebase.ref("promo/");

  adaRef.set({
    "1" : {
      "disccount" : "2000",
      "expirationDate" : "2018-05-69",
      "creationDate" : "2018-05-69",
    },
    2: {
      "disccount" : "21000",
      "expirationDate" : "2018-05-69",
      "creationDate" : "2015-05-69",
    },
  });

  adaRef = firebase.ref("problems/");

  adaRef.set({
    1: {
      desc: "Can't Login",
      user: "7nTnYvxd1ndjDVlPh3IFMc04Nrr2",
    },
    2: {
      desc: "Can't buy a painting",
      user: "UbjUbK3Av7RnfppYaW4ztzqh2Ag1"
    },
    3: {
      desc: "Can't register my credit card",
      user: "7nTnYvxd1ndjDVlPh3IFMc04Nrr2",
    },
  });

  adaRef = firebase.ref("user/");

  adaRef.set({
    "7nTnYvxd1ndjDVlPh3IFMc04Nrr2" : {
      "email" : "mau.m.perez@hotmail.com",
      "image" : "https://lh6.googleusercontent.com/-b9uQLeas5E4/AAAAAAAAAAI/AAAAAAAAAAA/ABtNlbCvHRN8IqWT5VPQ3s3qBAmE-pHmIw/mo/photo.jpg",
      "name" : "Mauricio Pérez"
    },
    "Aub75KQPO0efGY2DJCWW9s02IPG3" : {
      "email" : "yusomalo77@gmail.com",
      "image" : "https://lh6.googleusercontent.com/-18vnTfqanvU/AAAAAAAAAAI/AAAAAAAACG0/w2PJTF6mhiY/photo.jpg",
      "name" : "Yuso Malo"
    },
    "UbjUbK3Av7RnfppYaW4ztzqh2Ag1" : {
      "email" : "yusoazul77@hotmail.com",
      "image" : "https://graph.facebook.com/2161209663901638/picture",
      "name" : "Yuso Malo"
    },
  });

  adaRef = firebase.ref("admin/");

  adaRef.set({
    "7nTnYvxd1ndjDVlPh3IFMc04Nrr2" : {
      "email" : "mau.m.perez@hotmail.com",
      "id" : "7nTnYvxd1ndjDVlPh3IFMc04Nrr2",
      "image" : "https://lh6.googleusercontent.com/-b9uQLeas5E4/AAAAAAAAAAI/AAAAAAAAAAA/ABtNlbCvHRN8IqWT5VPQ3s3qBAmE-pHmIw/mo/photo.jpg",
      "name" : "Mauricio Pérez"
    },
    "Aub75KQPO0efGY2DJCWW9s02IPG3" : {
      "email" : "yusomalo77@gmail.com",
      "id" : "Aub75KQPO0efGY2DJCWW9s02IPG3",
      "image" : "https://lh6.googleusercontent.com/-18vnTfqanvU/AAAAAAAAAAI/AAAAAAAACG0/w2PJTF6mhiY/photo.jpg",
      "name" : "Yuso Malo"
    },
    "UbjUbK3Av7RnfppYaW4ztzqh2Ag1" : {
      "email" : "yusoazul77@hotmail.com",
      "id" : "UbjUbK3Av7RnfppYaW4ztzqh2Ag1",
      "image" : "https://graph.facebook.com/2161209663901638/picture",
      "name" : "Yuso Malo"
    },
    "b305XwnX5QZLl2dFit2gPOJR36i2" : {
      "email" : "a01377162@itesm.mx",
      "id" : "b305XwnX5QZLl2dFit2gPOJR36i2",
      "image" : "https://lh5.googleusercontent.com/-eYVOf4UT7rk/AAAAAAAAAAI/AAAAAAAAAAA/ABtNlbDIVw6bCPtYRo6Pu3dPwiEIjIscgw/mo/photo.jpg",
      "name" : "Guillermo Pérez Trueba"
    },
    "hfsP6GJrnzOX3MnmvoMofbuRFUv1" : {
      "email" : "adri130497@gmail.com",
      "id" : "hfsP6GJrnzOX3MnmvoMofbuRFUv1",
      "name" : "Adrian Gonzalez"
    },
    "q6BDuwfrJtPoSGW42u9V1xywdsg2" : {
      "email" : "a01371454@itesm.mx",
      "id" : "q6BDuwfrJtPoSGW42u9V1xywdsg2",
      "image" : "https://lh3.googleusercontent.com/-1OrYRrmSKJQ/AAAAAAAAAAI/AAAAAAAAAAA/ABtNlbDGmsZFGD9AOJmFrpWCcWP2zA2PAA/mo/photo.jpg",
      "name" : "José Antonio Malo De La Peña"
    },
  });

  adaRef = firebase.ref("client_support/");

  adaRef.set({
    "1" : {
      "clientId" : "7nTnYvxd1ndjDVlPh3IFMc04Nrr2",
      "date" : "2018-11-08",
      "problemId" : "1",
    },
    "2" : {
      "clientId" : "Aub75KQPO0efGY2DJCWW9s02IPG3",
      "date" : "2018-11-08",
      "problemId" : "3",
    },
    "3" : {
      "clientId" : "UbjUbK3Av7RnfppYaW4ztzqh2Ag1",
      "date" : "2014-11-08",
      "problemId" : "2",
    },
  });


    res.send('success');
});

app.use((req, res, next) =>  res.render('404')); //404
app.use((error, req, res, next) => res.render('500'));

// start the server at URL: http://localhost:3001/
app.listen(PORT, () => {
  console.log(`Our app is running on port ${ PORT }`);
});
